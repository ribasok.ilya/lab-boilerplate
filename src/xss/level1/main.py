import webapp2
import bleach

page_header = """
<!doctype html>
<html>
  <head>
    <!-- Internal game scripts/styles, mostly boring stuff -->
    <script src="https://xss-game.appspot.com/static/game-frame.js"></script>
    <link rel="stylesheet" href="https://xss-game.appspot.com/static/game-frame-styles.css" />
  </head>

  <body id="level1">
    <img src="https://xss-game.appspot.com/static/logos/level1.png">
      <div>
"""

page_footer = """
    </div>
  </body>
</html>
"""

main_page_markup = """
<form action="" method="GET">
  <input id="query" name="query" value="Enter query here..."
    onfocus="this.value=''">
  <input id="button" type="submit" value="Search">
</form>
"""

class MainPage(webapp2.RequestHandler):

  def render_string(self, s):
    self.response.out.write(s)

  def get(self):
    self.response.headers.add_header("X-XSS-Protection", "0")

    if not self.request.get('query'):
      self.render_string(page_header + main_page_markup + page_footer)
    else:
      query = self.request.get('query', '[empty]')

      query = bleach.clean(query)

      message = "Sorry, no results were found for <b>" + query + "</b>."
      message += " <a href='?'>Try again</a>."

      self.render_string(page_header + message + page_footer)

    return

application = webapp2.WSGIApplication([ ('.*', MainPage), ], debug=False)

def main():
    from paste import httpserver
    httpserver.serve(application, host='127.0.0.1', port='8080')

if __name__ == '__main__':
    main()

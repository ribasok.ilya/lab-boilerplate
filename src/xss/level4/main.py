import webapp2
import os
from webapp2_extras import jinja22
import bleach

class MainPage(webapp2.RequestHandler):
  @webapp2.cached_property
  def jinja2(self):
    return jinja22.get_jinja2(app=self.app)

  def render_response(self, path, **context):
    rv = self.jinja2.render_template(path, **context)
    self.response.write(rv)

  def get(self):
    self.response.headers.add_header("X-XSS-Protection", "0")



 #PROTECTION
    if not self.request.get('timer'):
      self.render_response('index.html')
    else:
      try:
        timerValue = int(self.request.get('timer', 0))
      except:
        timerValue = 1
      self.render_response('timer.html', timer=timerValue)

    return
   #PROTECTION



app = webapp2.WSGIApplication([ ('.*', MainPage), ], debug=False)

def main():
    from paste import httpserver
    httpserver.serve(app, host='127.0.0.1', port='8080')

if __name__ == '__main__':
    main()

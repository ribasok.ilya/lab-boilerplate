﻿/* eslint-disable */
function validateObject(array) {
  const errors = [];
  const phoneFormats = {
    USA: /^\(\d{3}\)-\d{3}-\d{4}$/,
    Germany: /^\d{4}-\d{7}$/,
  };

  function tryRegisterError(expectation, errorMessage) {
    if (!expectation) {
      errors.push(errorMessage);
    }
  }

  function isStringStartsWithUpperCase(element) {
    if (element === undefined) return false;
    return element[0] === element[0].toUpperCase() && typeof element === 'string';
  }

  function isNumber(age) {
    return typeof age === 'number';
  }

  function isPhoneNumber(number, country) {
    if (typeof number !== 'string') return false;

    if (phoneFormats[country] === undefined) {
      return number.match(/^\S{3,12}$/);
    }

    return number.match(phoneFormats[country]);
  }

  function isEmail(email) {
    return email.match(/\S+@\S+\.\S+/);
  }

  tryRegisterError(isStringStartsWithUpperCase(array.full_name), 'Full name isnt valid');
  // tryRegisterError(isStringStartsWithUpperCase(array.gender), 'Gender isnt valid');
  tryRegisterError(isStringStartsWithUpperCase(array.state), 'State isnt valid');
  tryRegisterError(isStringStartsWithUpperCase(array.country), 'Country isnt valid');
  tryRegisterError(isStringStartsWithUpperCase(array.note), 'Note isnt valid');
  tryRegisterError(isNumber(array.age), 'Age isnt valid');
  tryRegisterError(isPhoneNumber(array.phone, array.country), 'Phone isnt valid');
  tryRegisterError(isEmail(array.email), 'Email isnt valid');
  console.log(array);
  if (errors.length > 0) {
    errors.forEach((error) => console.log(error));
    return false;
  }
  console.log('Object is valid');
  return true;
}

export default validateObject;

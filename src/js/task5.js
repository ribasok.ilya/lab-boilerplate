﻿/* eslint-disable */
function find(array, field, value) {
  return array.find((item) => item[field] === value);
}

function findAll(array, field, value){
  return array.filter(item => item[field] === value).toArray();
}

export default find;

/* eslint-disable import/extensions */
/* eslint-disable no-console */

import { additionalUsers, randomUserMock } from './mock.js';
import getUnitedArray from './task1.js';
import validateObject from './task2.js';
import getFilteredArray from './task3.js';
import getSortedArray from './task4.js';
import find from './task5.js';
import findAll from './task5.js';
import getPercentage from './task6.js';

let allTeachersArray = [];
let tempTeachersArray = allTeachersArray;
let favoriteTeachers = [];

const countrySelection = document.getElementById('countrySelection');
const ageSelection = document.getElementById('ageSelection');
const genderSelection = document.getElementById('genderSelection');

var switchSort = false;

function initSearchTeacher() {
  const searchSelection = document.getElementById('search_types_selection');
  const searchInput = document.getElementById('search_teacher_input');
  const searchButton = document.getElementById('search_teacher_button');

  searchButton.addEventListener('click', () => {
    let users = [];

    if (searchSelection.value === 'Search by name'){
      users = [];
      users.push(findAll(allTeachersArray, 'full_name', searchInput.value));
    }
    else if (searchSelection.value === 'Search by notes'){
      users = [];
      users.push(findAll(allTeachersArray, 'notes', searchInput.value));
    }
    else if (searchSelection.value === 'Search by age'){
      users = [];
      users.push(findAll(allTeachersArray, 'age', parseInt(searchInput.value)));
    }

    showUsers(users);
  });
}

function showUsers(users) {
  const usersLimit = 10;

  let usersToShowAmount = users.length > usersLimit ? usersLimit : users.length;

  tempTeachersArray = [];

  cleanInnerHTML('cardsGrid');

  for (let i = 0; i < usersToShowAmount; i++) {
    document.getElementsByClassName('cardsGrid')[0].innerHTML += `
            <div class="card" id="user${i}">
            <img src="images/star.svg" class="star" alt="star" id="star${i}">
            <div class="cardScale">
                <a href="#uInfoModal"><img src=${users[i].picture_large} alt="teacher photo"></a>
            </div>
            <p class="cardName">${users[i].full_name}</p>
            <p class="cardCountry">${users[i].country}</p>
            </div>`;

    tempTeachersArray.push(users[i]);
  }

  showStatisticsTable(tempTeachersArray);

  $('.card')
    .on('click', showUserInfo);
  $('.star')
    .on('click', toggleTeacherToFavorite);
}

function showTeachers() {
  showUsers(allTeachersArray);
}

function initTeachersCountrySelection() {

  countrySelection.addEventListener('change', () => {
    tempTeachersArray = getTeachersFilteredByAllSelections(allTeachersArray);

    showUsers(tempTeachersArray);
  });
}

function getTeachersFilteredByCountrySelection (teachers, countrySelection) {
  let teachersExample = teachers;

  if (countrySelection.value.toString() !== "All Countries") {
    teachersExample = getFilteredArray(teachersExample, 'country', countrySelection.value.toString());
  }

  return teachersExample;
}

function initTeachersAgeSelection() {

  ageSelection.addEventListener('change', () => {
    tempTeachersArray = getTeachersFilteredByAllSelections(allTeachersArray);

    showUsers(tempTeachersArray);
  });
}

function getTeachersFilteredByAgeSelection (teachers, ageSelection) {
  let teachersExample = teachers;

  if (ageSelection.value.toString() === 'Younger than 30') {
    teachersExample = teachersExample.filter((element) => element.age < 30);
  } else if (ageSelection.value.toString() === '30 to 50') {
    teachersExample = teachersExample.filter((element) => element.age >= 30 && element.age < 50);
  } else if (ageSelection.value.toString() === 'Older than 50') {
    teachersExample = teachersExample.filter((element) => element.age >= 50);
  }

  return teachersExample;
}

function initTeachersGenderSelection() {

  genderSelection.addEventListener('change', () => {
    tempTeachersArray = getTeachersFilteredByAllSelections(allTeachersArray);

    showUsers(tempTeachersArray);
  });
}

function getTeachersFilteredByGenderSelection (teachers, genderSelection) {
  let teachersExample = teachers;

  if (genderSelection.value.toString() !== "All Genders") {
    teachersExample = getFilteredArray(teachersExample, 'gender', genderSelection.value.toString().toLowerCase());
  }

  return teachersExample;
}

function getTeachersFilteredByAllSelections(teachers){
  let teachersExample = teachers;

  teachersExample = getTeachersFilteredByCountrySelection(teachersExample, countrySelection);
  teachersExample = getTeachersFilteredByAgeSelection(teachersExample, ageSelection);
  teachersExample = getTeachersFilteredByGenderSelection(teachersExample, genderSelection);

  return teachersExample;
}

function showUserInfo() {
  let full_id = $(this).attr('id');
  let id = full_id.split('user').pop();
  let currentUser = tempTeachersArray[id];

  document.getElementsByClassName('userInfoModal')[0].innerHTML += `
       <a href=""> <span class="close">&times;</span></a>
                  <div class="popupContainer">
                    <div class="title">Teacher info</div>
                    <div class="photoAndInfo">
                      <img src=${currentUser.picture_large} alt="teacher photo">
                        <div class="descr">
                          <div class="title">${currentUser.full_name}</div>
                          <div class="cardData">
                            <p>${currentUser.country}</p>
                            <p>${currentUser.age}, ${currentUser.gender}</p>
                            <p>${currentUser.email}</p>
                            <p>${currentUser.phone}</p>
                          </div>
                        </div>
                    </div>
                    <p class="teacherInfo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet cum dolor doloribus
                      ipsum iure libero, voluptates! Blanditiis dolorem eius esse facere, iusto necessitatibus non officia
                      omnis quaerat recusandae reiciendis sapiente?</p>
                    <p class="toggleMap"><a href="#">toggle map</a></p>
                  </div>
    `;
}

function showStatisticsTable(users){
  cleanInnerHTML('table_statistics');

  const usersLimit = 10;

  let usersToShowAmount = users.length > usersLimit ? usersLimit : users.length;

  let out = '';
  out +=`<tr>`;
  out +=`<th id="table_statistics_name">Name</th>`;
  out +=`<th id="table_statistics_age">Age</th>`;
  out +=`<th>Gender</th>`;
  out +=`<th id="table_statistics_nationality">Nationality</th>`;
  out +=`</tr>`

  for (let i = 0; i < usersToShowAmount; i++) {
    out +=`<tr>`;
    out +=`<td>${users[i].full_name}</td>`;
    out +=`<td>${users[i].age}</td>`;
    out +=`<td>${users[i].gender}</td>`;
    out +=`<td>${users[i].country}</td>`;
    out +=`</tr>`;
  }
  $('.table_statistics').html(out);

  initAllTableSorts();
}

function initAllTableSorts(){
  initSortByName();
  initSortByAge();
  initSortByNationality();
}

function initSortByName() {
  const tableName = document.getElementById('table_statistics_name');
  let sortedTeachersArray = tempTeachersArray;

  tableName.addEventListener('click', () => {
    switchSort = !switchSort;
    sortedTeachersArray = getSortedArray(sortedTeachersArray, 'full_name', switchSort);
    showStatisticsTable(sortedTeachersArray);

    initAllTableSorts(); // Need, because we recreate table header on every repaint, so we should find a new one
  });
}

function initSortByAge() {
  const tableName = document.getElementById('table_statistics_age');
  let sortedTeachersArray = tempTeachersArray;

  tableName.addEventListener('click', () => {
    switchSort = !switchSort;
    sortedTeachersArray = getSortedArray(sortedTeachersArray, 'age', switchSort);
    showStatisticsTable(sortedTeachersArray);

    initAllTableSorts(); // Need, because we recreate table header on every repaint, so we should find a new one
  });
}

function initSortByNationality() {
  const tableName = document.getElementById('table_statistics_nationality');
  let sortedTeachersArray = tempTeachersArray;

  tableName.addEventListener('click', () => {
    switchSort = !switchSort;
    sortedTeachersArray = getSortedArray(sortedTeachersArray, 'country', switchSort);
    showStatisticsTable(sortedTeachersArray);

    initAllTableSorts(); // Need, because we recreate table header on every repaint, so we should find a new one
  });
}

function toggleTeacherToFavorite() {
  let full_id = $(this).attr('id');
  let id = full_id.split('star').pop();
  let currentUser = tempTeachersArray[id];

  if (!isIncludeFavoriteTeacher(favoriteTeachers, currentUser))
    addToFavorite();
  else
    removeFromFavorite();

  saveFavoriteTeachers();
  showFavoriteTeachers();

  function isIncludeFavoriteTeacher(teachersArr, teacher) {
    for (let i = 0; i < teachersArr.length; i++) {
      if (teachersArr[i].phone === teacher.phone) {
        return true;
      }
    }
    return false;
  }

  function addToFavorite(){
    tempTeachersArray[id].favorite = true;
    favoriteTeachers.push(currentUser);
    $(`#${full_id}`).css('opacity', '1');
  }

  function removeFromFavorite(){
    tempTeachersArray[id].favorite = false;
    favoriteTeachers = favoriteTeachers.filter(x=>x.phone!==currentUser.phone);
    $(`#${full_id}`).css('opacity', '0.25');
  }
}

function initAddNewTeacher() {

  const addNewTeacherButton = document.getElementById('addTeacherSubmitButton');

  function getGender() {
    let gender = '';
    const radios = document.getElementsByName('gender');

    radios.forEach((element) => {
      if (element.checked) {
        gender = element.value;
      }
    });

    return gender;
  }

  addNewTeacherButton.addEventListener('click', () => {

    let gender = getGender();

    const user = {
      full_name: document.getElementById('addTeacherModal_name').value,
      gender: gender,
      note: 'Standard note',
      state: 'Standard state',
      country: document.getElementById('addTeacherModal_country').value,
      city: document.getElementById('addTeacherModal_city').value,
      email: document.getElementById('addTeacherModal_email').value,
      phone: document.getElementById('addTeacherModal_phone').value,
      age: Number.parseInt(document.getElementById('addTeacherModal_age').value),
    };

    if (validateObject(user)) {
      allTeachersArray.push(user);
      saveAllTeachers();
    }
  });
}

function saveAllTeachers(){
  localStorage.setItem('allTeachersArray', JSON.stringify(allTeachersArray));
}

function loadAllTeachers() {
  if (localStorage.getItem('allTeachersArray')) {
    allTeachersArray = JSON.parse(localStorage.getItem('allTeachersArray'));
  }

  if (allTeachersArray.length === 0) {
    allTeachersArray = getUnitedArray(randomUserMock, additionalUsers);
  }

  showFavoriteTeachers();
}

function loadFavoriteTeachers() {
  if (localStorage.getItem('favoriteTeachers')) {
    favoriteTeachers = JSON.parse(localStorage.getItem('favoriteTeachers'));
    showFavoriteTeachers();
  }
}

function saveFavoriteTeachers() {
  localStorage.setItem('favoriteTeachers', JSON.stringify(favoriteTeachers));
}

function showFavoriteTeachers(){
  if (favoriteTeachers.length === 0){
    cleanInnerHTML('favoriteTeachers');
  }
  else {
    cleanInnerHTML('favoriteTeachers');

    for (let i = 0; i < favoriteTeachers.length; i++) {
      document.getElementsByClassName('favoriteTeachers')[0].innerHTML += `
            <div class="card" id="favoriteTeacher${i}">
            <div class="cardScale">
                <a href="#uInfoModal"><img src=${favoriteTeachers[i].picture_large} alt="teacher photo"></a>
            </div>
            <p class="cardName">${favoriteTeachers[i].full_name}</p>
            <p class="cardCountry">${favoriteTeachers[i].country}</p>
            </div>`;
    }
  }
}

function cleanInnerHTML(className){
  document.getElementsByClassName(className)[0].innerHTML = '';
}

$(document).ready(function () {
  // favoriteTeachers = [];
  // saveFavoriteTeachers();
  loadAllTeachers();
  loadFavoriteTeachers();
  showTeachers();
  initTeachersCountrySelection();
  initTeachersAgeSelection();
  initTeachersGenderSelection();

  initAllTableSorts();

  initAddNewTeacher();
  initSearchTeacher();
});


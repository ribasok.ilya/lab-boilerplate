﻿/* eslint-disable */
function getFilteredArray(array, field, value) {
  return array.filter((item) => item[field] === value);
}
export default getFilteredArray;

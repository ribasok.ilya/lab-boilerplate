﻿/* eslint-disable */
function getPercentage(array, comparsionOperation, age) {
  let result;
  switch (comparsionOperation) {
    case '=':
      result = array.filter((element) => (element.age) === age).length;
      break;
    case '>':
      result = array.filter((element) => element.age < age).length;
      break;
    case '<':
      result = array.filter((element) => element.age > age).length;
      break;
    default:
      console.log('Wrong operaion');
      result = 0;
  }
  const percents = (result / array.length) * 100;
  return percents.toFixed(1);
}

export default getPercentage;

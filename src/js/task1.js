﻿/* eslint-disable */
function convertMockData(mock) {
  const randUserMockConvert = [];
  mock.forEach((target) => {
    randUserMockConvert.push(
      {
        gender: String(target.gender),
        title: String(target.name.title),
        full_name: `${target.name.first} ${target.name.last}`,
        city: String(target.location.city),
        state: String(target.location.state),
        country: String(target.location.country),
        postcode: Number(target.location.postcode),
        coordinates: {
          latitude: String(target.location.coordinates.latitude),
          longitude: String(target.location.coordinates.longitude),
        },
        timezone: {
          offset: String(target.location.timezone.offset),
          description: String(target.location.timezone.description),
        },
        email: String(target.email),
        b_date: String(target.dob.date),
        age: Number(target.dob.age),
        phone: String(target.phone),
        picture_large: String(target.picture.large),
        picture_thumbnail: String(target.picture.thumbnail),
      },
    );
  });
  return randUserMockConvert;
}

function addValues(array) {
  const idRange = 100;

  function getRandArrayValue(randArray) {
    return randArray[Math.floor(Math.random() * randArray.length)];
  }

  const randData = [{
    favorite: [true, false],
    course: ['Math', 'Programming', 'English'],
    bg_color: ['#ffffff', '#000000', '#fff000'],
    note: ['Bad', 'Neutral', 'Good'],
  }];
  array.forEach((element) => {
    element.id = `ID${array.indexOf(element)}${Math.round(Math.random() * idRange)}`;
    element.favorite = false; /*getRandArrayValue(randData[0].favorite);*/
    element.course = getRandArrayValue(randData[0].course);
    element.bg_color = getRandArrayValue(randData[0].bg_color);
    element.note = getRandArrayValue(randData[0].note);
  });
  return array;
}

function uniteArrays(firstArr, secondArr) {
  secondArr.forEach((secondArrElement) => {
    // eslint-disable-next-line max-len
    if (!firstArr.find((firstArrayElement) => firstArrayElement.full_name === secondArrElement.full_name)) {
      firstArr.push(secondArrElement);
    }
  });
  return firstArr;
}

function getUnitedArray(randUserMock, additionalUsers) {
  let users = convertMockData(randUserMock);
  users = addValues(users);
  users = uniteArrays(users, additionalUsers);
  return users;
}

export default getUnitedArray;

﻿/* eslint-disable */
function getSortedArray(array, field, isDescending = false) {
  return array.sort((a, b) => {
    if (a[field] > b[field]) {
      return isDescending ? -1 : 1;
    }
    if (a[field] < b[field]) {
      return isDescending ? 1 : -1;
    }
    return 0;
  });
}

export default getSortedArray;

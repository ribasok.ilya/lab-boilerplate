﻿/* eslint-disable import/extensions */
/* eslint-disable no-console */
import { additionalUsers, randomUserMock } from './mock.js';
import getUnitedArray from './task1.js';
import validateObject from './task2.js';
import getFilteredArray from './task3.js';
import getSortedArray from './task4.js';
import find from './task5.js';
import getPercentage from './task6.js';

console.log('Task 1');
const usersArray = getUnitedArray(randomUserMock, additionalUsers);
console.log(usersArray);

console.log('Task 2');
const isValid = validateObject(usersArray[0]);
console.log(isValid);

console.log('Task 3');
const filteredArray = getFilteredArray(usersArray, 'gender', 'male');
console.log(filteredArray);

console.log('Task 4');
const sortedArray = getSortedArray(usersArray, 'b_day', true);
console.log(sortedArray);

console.log('Task 5');
const findResult = find(usersArray, 'full_name', 'Beatrice Bergeron');
console.log(findResult);

console.log('Task 6');
const percents = getPercentage(usersArray, '>', 30);
console.log(percents, '%');
